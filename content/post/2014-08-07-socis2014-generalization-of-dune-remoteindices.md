---
title: SOCIS 2014 Generalization of Dune::RemoteIndices
date: 2014-08-07
tags: ["SOCIS", "C++11"," DUNE", "HPC", "MPI", "Parallel Programming", "Thread"]
---

Hi everybody,\
I am back :) In the last weeks I worked on the re-factoring of Dune class RemoteIndices. See
[here]({{<ref "2014-06-18-socis2014-dune-parallel-communication">}}) for more details about this class and its
purpose.

In the original implementation RemoteIndices stored all the data relative to the indices present on remote processes
and it contained all the methods to compute them. It supported only MPI as parallel paradigm therefore it was strongly
coupled with MPI functions and types.

Obviously, this coupling needed to be removed in order to have the possibility to use different parallel paradigms (e.g.
thread). Therefore I re-wrote this class separating the MPI part from the rest and you can find the source code in my
local branch [remoteindices](https://github.com/magnese/dune-common/tree/remoteindices).

More specifically the files which I created are

- [mpiparallelparadigm.hh](https://github.com/magnese/dune-common/blob/remoteindices/dune/common/parallel/mpiparallelparadigm.hh)
- [remoteindices.hh](https://github.com/magnese/dune-common/blob/remoteindices/dune/common/parallel/remoteindices.hh)

while the same toy example to test this new implementation can be found here

- [mpiremoteindicestest.cc](https://github.com/magnese/dune-common/blob/remoteindices/dune/common/parallel/test/mpiremoteindicestest.cc).

All the MPI-specific implementations are contained in the first file. More precisely, there is a new class,
Dune::MPIParadigm, which is templated against the ParallelIndexSet. Its constructor accepts an MPI communicator.

In the second file there is the definition of the new Dune::RemoteIndices class which now is templated against the
parallel paradigm which we want to use and the constructor accepts an object of that type.

Therefore, now RemoteIndices is agnostic from the parallel paradigm which we want to use. It stores only all the data
and have only local methods.

RemoteIndices builds the remote indices calling the method buildRemote() of the templated argument therefore each
parallel paradigm implementation must implement this method.

For compatibility reason with some old classes it is still possible to extract the communicator from RemoteIndices but,
since this information doesn't make sense in all the possible parallel paradigms, this feature will be removed.

Now, I have started to write a ThreadParadigm which can be used instead of the MPI implementation to build the remote
indices.

For any questions or suggestions please write me.

Stay tuned!\
Marco.
