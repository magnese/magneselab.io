---
title: SOCIS 2014 Thread-paradigm implementation for Dune::RemoteIndices
date: 2014-08-14
tags: ["SOCIS", "C++11"," DUNE", "HPC", "MPI", "Parallel Programming", "Thread"]
---

Hi everybody,\
I have finished the implementation of the thread-paradigm for the class RemoteIndices. Now it is possible to construct
the remote indices both in the MPI scenario that in the thread scenario. In the usual branch
[remoteindices](https://github.com/magnese/dune-common/tree/remoteindices) there is a new header file

- [threadparallelparadigm.hh](https://github.com/magnese/dune-common/blob/remoteindices/dune/common/parallel/threadparallelparadigm.hh)

which contains the implementation of the thread-paradigm and the same toy example to test this new implementation using
thread instead of MPI can be found here

- [threadremoteindicestest.cc](https://github.com/magnese/dune-common/blob/remoteindices/dune/common/parallel/test/threadremoteindicestest.cc).

Stay tuned!\
Marco.
