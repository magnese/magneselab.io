---
title: SOCIS 2014 Dune::ThreadCommunicator improved
date: 2014-09-05
tags: ["SOCIS", "C++11"," DUNE", "HPC", "MPI", "Parallel Programming", "Thread"]
---

Hi everybody,\
I have improved the class for the communication. See my previous post
[here]({{<ref "2014-09-03-socis2014-dune-thread-communicator.md">}}) for more details.

I have finally decoupled the interface from the parallel implementation underneath. The interface is class
Dune::Communicator which is present in the following header

- [communicator.hh](https://github.com/magnese/dune-common/blob/remoteindices/dune/common/parallel/communicator.hh)

while the two available implementation are Dune::ThreadCommunicator (described in the previous post) and
Dune::MPICommunicator (based on the old Dune::BufferedCommunicator),

- [threadcommunicator.hh](https://github.com/magnese/dune-common/blob/remoteindices/dune/common/parallel/threadcommunicator.hh)
- [mpicommunicator.hh](https://github.com/magnese/dune-common/blob/remoteindices/dune/common/parallel/mpicommunicator.hh).

Therefore, in order to use the MPI version the user will create the class

- Dune::Communicator\<Dune::MPICommunicator\<I\>\>

where I is the type of the interface. Analogously, for the thread version, the declaration will be

- Dune::Communicator\<Dune::ThreadCommunicator\<I\>\>.

A part for this, I have also implemented the support for variable size data structures also for the thread implementation.

Now I have a fully implemented, for both MPI and thread, all the classes which manage the parallel infrastructure of
dune-common (which are used also in dune-istl). Moreover the new structure is very flexible since the interfaces and all
the sequential parts are independent from the parallel paradigm used. Therefore, in order to add new parallel paradigms
is sufficient to implement only several methods which will provide the effective exchange of data among the processes.

I am quite sad because the SOCIS project is over :(\
But don't worry I will keep working on this project :) Therefore stay tuned and check for new "very exciting" updates!!

Marco.
