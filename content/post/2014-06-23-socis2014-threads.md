---
title: SOCIS 2014 Threads
date: 2014-06-23
tags: ["SOCIS", "C++11"," DUNE", "HPC", "MPI", "Parallel Programming", "Thread"]
---

Hi everybody,\
welcome back!

The objective of the SOCIS project is to add support for threads in dune-common in order to have an hybrid MPI-threads
approach to boost up performance. There are a three different thread models which can be used:

- [POSIX threads](http://pubs.opengroup.org/onlinepubs/7908799/xsh/threads.html);
- external libraries ([boost](http://www.boost.org/), [OpenMP](http://openmp.org/wp/),
  [Intel TBB](http://www.threadingbuildingblocks.org/));
- [C++11 threads](http://en.cppreference.com/w/cpp/thread).

The choice of the C++11 seems the most reasonable for several reasons:

1. no need of additional libraries;
2. standardized;
3. type-safe;
4. perfect integration with C++ (use of lambdas, functors and no need of using macros and void\*);
5. simply notation;
6. different levels of concurrency support, from lowest level to the highest, atomic operations, threads and task;
7. every C++ developer is familiar with it therefore it is more simple for everyone to contribute to the code without
   spending time learning new libraries.

The only big drawback of this approach is the fact that it is necessary a certain version of the compilers. Indeed DUNE
2.3.1 needs to be compatible with g++ >= 4.4, Clang >= 3.2 and ICC >= 13. Unfortunately these compilers have a very
limited support for C++11 threads. More precisely:

- [Clang 3.2](http://clang.llvm.org/cxx_status.html) (Dec 2012) supports almost everything;
- [ICC 13](https://software.intel.com/en-us/articles/c0x-features-supported-by-intel-c-compiler) (Sept 2012) has a very
  limited support;
- [GCC 4.4](https://gcc.gnu.org/gcc-4.4/cxx0x_status.html) (Apr 2009) has a very limited support.

A possible way to address this problem is to activate the usage of threads only for compilers which support it (gcc >=
4.8 and clang >= 3.3). Nevertheless the performance boost gained with threads is most important on supercomputer which
have good multi-threading possibilities and on these computers the backwards compatibility is crucial.

For this reason I will use a different approach which won't break the backwards compatibility: when the compiler
supports it, the code will use C++11, when it doesn't, the code will use
[TinyThread++1.1](http://tinythreadpp.bitsnbites.eu/). This library consists of only 3 files and it supports a subset of
C++11 capabilities.&nbsp;Since it is so small, it can be directly included in the source of DUNE.

In order to have a simple idea of how the C++11 threads work, I have implemented the same toy example of the previous
post. You can find the code in the same github [repository](https://github.com/magnese/dune-common) (branch threads) at
the following link

- [threadstest.cc](https://github.com/magnese/dune-common/blob/threads/dune/common/parallel/test/threadstest.cc).

The code threadstest.cc performs almost the same operations of the code paralleltest.cc. It is important to notice that
now the memory is shared and therefore there are no more local vectors al. The threads have access to the same global
vector a. The usage of a mutex mtx avoids a race condition when using the same resources (namely the cout). &nbsp;For
what concern the writing on a there is no race condition since there is no overlapping between threads.

An excellent but concise reference for C++11 threads is
[The C++ Programming Language (4th Edition), Bjarne Stroustrup](http://www.stroustrup.com/4th.html") while a simpler
book is [C++ Concurrency in Action - Practical Multithreading, Anthony Williams](http://www.manning.com/williams/).

The first goal now is to create an interface to avoid a direct usage of threads by the user and to have something
similar to the MPI interface described in the previous post.

Stay tuned!\
Marco.
