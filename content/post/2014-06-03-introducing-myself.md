---
title: Introducing myself
date: 2014-06-03
---

Hello,\
my name is Marco Agnese and I am a first year PhD student at Imperial College London in applied mathematics where I am
working on numerical analysis of geometric PDE.

My work consist in implementing FEM schemes to solve interface evolution problems of bi-phase fluids in 2D and 3D
domains using a fitted approach.

I took my bachelor and master in applied mathematics at Politecnico di Torino (Italy) and for my master thesis project
I worked almost a year at Los Alamos National Lab (New Mexico) where I have implemented a parallel code in MPI and C++
to solve equilibrium problem using spectral elements. This work is part of LAPS (Los Alamos Plasma Physic Simulator).

Stay tuned!\
Marco.
