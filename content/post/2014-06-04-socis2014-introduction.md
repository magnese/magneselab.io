---
title: SOCIS 2014 Introduction
date: 2014-06-04
tags: ["SOCIS", "C++11"," DUNE", "HPC", "MPI", "Parallel Programming", "Thread"]
---

Hi,\
I am very pleased to announce that I have been selected for the 2014 edition of
[SOCIS](http://sophia.estec.esa.int/socis2014/?q=about). ESA Summer of Code in Space (SOCIS) is a program run by the
[European Space Agency](http://www.esa.int/). It aims at offering student developers stipends to write code for various
space-related open source software projects. Through SOCIS, accepted student applicants are paired with a mentor or
mentors from the participating projects.

For the next 3 month I will work for the [DUNE](http://www.dune-project.org/index.html) framework. The Distributed and
Unified Numerics Environment (DUNE) is a modular toolbox for solving partial differential equations with grid-based
methods. It supports the easy implementation of methods like Finite Elements, Finite Volumes and also Finite
Differences.

My project is to add thread support to parallel index sets (see
[project 1](http://users.dune-project.org/projects/esa-socis-2014/wiki/Project_ideas)). In parallel scientific
computing the entries of containers can be distinguished in entries that never get communicated and entries that do
get exchanged with other cores/processors.

In the module dune-common there is an abstraction for this that allows users to write parallel algorithms without having
to deal with [MPI](http://www.mcs.anl.gov/research/projects/mpi/) or other parallel mode. Currently only MPI is
supported. The idea of this project is to support also thread parallelism and optimally a hybrid approach of both (MPI
and threads).

The mentors of the project are [Dr. Markus Blatt](http://www.dr-blatt.de/english/about-me/) and
[Prof. Christian Engwer](http://wwwmath.uni-muenster.de/u/christian.engwer/).

Stay tuned!\
Marco.
