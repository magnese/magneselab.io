---
title: PhD project
#subtitle:
comments: false
---

# Research interests

- Numerical Analysis, (Nonlinear) Partial Differential Equations, Finite Element Approximations
- Geometric Evolution Equations, Free Boundary Problems, Two-Phase Navier-Stokes flow
- Scientific Computing, High Performance Computing

# Thesis

- [Front tracking finite element methods for two-phase Navier--Stokes flow](https://spiral.imperial.ac.uk/handle/10044/1/68274),\
  1 February 2018, Imperial College London, London, UK.\
  \[[Source](https://github.com/magnese/reports/tree/master/phd_thesis)\]

# Publications

- M. Agnese and R. Nürnberg:\
  [Fitted finite element discretization of two-phase Stokes flow](https://onlinelibrary.wiley.com/doi/abs/10.1002/fld.4237),\
  Internat. J. Numer. Methods Fluids 82 (2016), 709--729.\
  \[[Open access](https://spiral.imperial.ac.uk/handle/10044/1/30442)\]
  \[[Source](https://github.com/magnese/reports/tree/master/papers/fitted_stokes)\]

- M. Agnese and R. Nürnberg:\
  [Fitted front tracking methods for two-phase incompressible Navier--Stokes flow: Eulerian and ALE finite element
  discretizations](http://www.math.ualberta.ca/ijnam/Volume-17-2020/No-5-20/2020-05-01.pdf),\
  Int. J. Numer. Anal. Model. 17 (2020), 613--642.\
  \[[Source](https://github.com/magnese/reports/tree/master/papers/fitted_navier_stokes)\]

# Talks

- [Fitted Finite Element Discretization of Two-Phase \(Navier--\) Stokes Flow](http://imperialsiam.com/events/themed-events),\
  9 March 2016, SIAM Student Chapter, PDE Theme Day, Imperial College London, London, UK.\
 \[[Abstract](https://github.com/magnese/reports/tree/master/talks/pde_day_imperial_16/abstract_pde_day.pdf)\]
 \[[Slides](https://github.com/magnese/reports/tree/master/talks/pde_day_imperial_16/pde_day.pdf)\]
 \[[Source](https://github.com/magnese/reports/tree/master/talks/pde_day_imperial_16)\]

- [Fitted Finite Element Discretization of Two-Phase Navier--Stokes Flow](http://www.siamoxford.com/conference-2016),\
  27 April 2016, The 8th Oxford University SIAM Student Chapter Conference, University of Oxford, Oxford, UK.\
  \[[Abstract](https://github.com/magnese/reports/tree/master/talks/siam_oxford_16/abstract_siam_oxford.pdf)\]
  \[[Slides](https://github.com/magnese/reports/tree/master/talks/siam_oxford_16/siam_oxford.pdf)\]
  \[[Source](https://github.com/magnese/reports/tree/master/talks/siam_oxford_16)\]

- [Fitted ALE scheme for Two-Phase Navier--Stokes Flow](http://people.brunel.ac.uk/~icsrsss/bicom/mafelap2016),\
  14-17 June 2016, The Mathematics Of Finite Elements And Applications 2016 \(MAFELAP 2016\), Brunel University London,
  London, UK.\
  \[[Abstract](https://github.com/magnese/reports/tree/master/talks/mafelap_16/abstract_mafelap.pdf)\]
  \[[Slides](https://github.com/magnese/reports/tree/master/talks/mafelap_16/mafelap.pdf)\]
  \[[Source](https://github.com/magnese/reports/tree/master/talks/mafelap_16)\]

- [Fitted Finite Element Discretization of Two-Phase Navier-Stokes Flow](http://www.imperial.ac.uk/fluids-cdt/symposium),\
  28 June 2016, CDT Fluids Student Symposium 2016, Imperial College London, London, UK.\
  \[[Abstract](https://github.com/magnese/reports/tree/master/talks/cdt_fluids_16/abstract_cdtfluids.pdf)\]
  \[[Slides](https://github.com/magnese/reports/tree/master/talks/cdt_fluids_16/cdtfluids.pdf)\]
  \[[Source](https://github.com/magnese/reports/tree/master/talks/cdt_fluids_16)\]

- [FEM approximation of Two-Phase Navier--Stokes Flow using DUNE-FEM](http://www2.warwick.ac.uk/fac/sci/maths/research/events/2015-16/nonsymposium/pde),\
  4-8 July 2016, PDE Software Frameworks 2016, The University of Warwick, Coventry, UK.\
  \[[Abstract](https://github.com/magnese/reports/tree/master/talks/pdesoft_16/abstract_pdesoft.pdf)\]
  \[[Slides](https://github.com/magnese/reports/tree/master/talks/pdesoft_16/pdesoft.pdf)\]
  \[[Source](https://github.com/magnese/reports/tree/master/talks/pdesoft_16)\]

- [Fitted ALE scheme for Two-Phase Navier--Stokes Flow](http://www.mit.jyu.fi/scoma/cmam2016),\
  31 July-6 August 2016, Computational Methods in Applied Mathematics \(CMAM-7\), University of Jyväskylä, Finland.\
  \[[Abstract](https://github.com/magnese/reports/tree/master/talks/cmam7_16/abstract_cmam7.pdf)\]
  \[[Slides](https://github.com/magnese/reports/tree/master/talks/cmam7_16/cmam7.pdf)\]
  \[[Source](https://github.com/magnese/reports/tree/master/talks/cmam7_16)\]

- [Fitted ALE scheme for Two-Phase Navier--Stokes Flow](http://www.imperial.ac.uk/fluids-cdt/uk-fluids-conference-2016),\
  7-9 September 2016, Inaugural UK Fluids Conference, Imperial College London, London, UK.\
  \[[Abstract](https://github.com/magnese/reports/tree/master/talks/uk_fluids_16/abstract_uk_fluids.pdf)\]
  \[[Slides](https://github.com/magnese/reports/tree/master/talks/uk_fluids_16/uk_fluids.pdf)\]
  \[[Source](https://github.com/magnese/reports/tree/master/talks/uk_fluids_16)\]
